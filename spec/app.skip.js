import React from 'react';
import {mount} from 'enzyme';
import App from '../src/react_components/app.jsx';
describe('App (ReactComponent)', () => {
  const app = mount( <App withoutMap={true} /> );

  it('should render wrapper div.app', () => {
    expect(app.find('.app').length).toEqual(1)
  });

  it('should render menu', () => {
    expect(app.find('.menu_absolute').length).toEqual(1)
  });

  it('should render dropzone selector', () => {
    expect(app.find('.dropzone-selector').length).toEqual(1)
  });

  describe('changing winds type', () => {
    it('clicking on winds tab (flag) should open winds tab', () => {
      app.find('.flag').simulate('click');
      expect(app.find('.levels-selector').length).toEqual(1);
    });

    it('should set change state.selectedWindsType', () => {
      expect(app.state('selectedLevelsType')).toEqual('jeppesen');
      app.find('.flag').simulate('click');
      app.find('.levels-selector').find('.custom-levels-select').simulate('click');
      expect(app.state('selectedLevelsType')).toEqual('custom')
    });
  });

  describe('JumpersController', () => {
    beforeEach(() => {
      app.find('.user').simulate('click');
    });
    it('clicking on jumpers tab (user) should open jumpers tab', () => {
      expect(app.find('.dropzone-controller').find('button').find('.plus').length).toEqual(1);
    });
  });

  describe('changing dropzone', () => {
    it('should set change state.selectedDropzone', () => {
      // Needs to wait for Ajax
      //app.find('.item').findWhere((a) => a.text() == 'Utti').simulate('click');
    });
  })
});


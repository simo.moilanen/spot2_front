import Level from "../../src/models/level.js";

describe("Level", () => {
  describe('constuctor()', () => {
    it("should construct without parameters", () => {
      const level = new Level();
      expect(level.id).not.toBeDefined();
    });
    it("should construct with imperial values", () => {
      const level = new Level({id: 1, height_ft: 1000, wind_direction_deg: 240, wind_speed_kt: 10, temperature_c: 5});
      expect(level.id).toEqual(1);
      expect(level.height_m).toEqual(304.8);
      expect(level.wind_direction_deg).toEqual(240);
      expect(level.wind_speed_ms).toEqual(5.144);
      expect(level.temperature_c).toEqual(5);
    });
    it("should construct with SI values", () => {
      const level = new Level({id: 1, height_m: 1000, wind_direction_deg: 240, wind_speed_ms: 10, temperature_c: 5});
      expect(level.id).toEqual(1);
      expect(level.height_m).toEqual(1000);
      expect(level.wind_direction_deg).toEqual(240);
      expect(level.wind_speed_ms).toEqual(10);
      expect(level.temperature_c).toEqual(5);
    });
    it("should take height_ft = 0 and set height_m = 0", () => {
      const level = new Level({height_ft: 0});
      expect(level.height_m).toEqual(0);
    });
  });

  describe('updateAttribute()', () => {
    it("should take height_ft and change height_m", () => {
      const level = new Level();
      level.updateAttribute('height_ft', 1000);
      expect(level.height_m).toEqual(304.8);
    });
    it("should take wind_speed_kt and change wind_speed_ms", () => {
      const level = new Level();
      level.updateAttribute('wind_speed_kt', 10);
      expect(level.wind_speed_ms).toEqual(5.144);
    });
    it("should take wind_direction_deg and change wind_direction_deg", () => {
      const level = new Level();
      level.updateAttribute('wind_direction_deg', 10);
      expect(level.wind_direction_deg).toEqual(10);
    });
  });

  describe('direction_rad()', () => {
    it('should return direction in radians', () => {
      const level = new Level({wind_direction_deg: 180});
      expect(level.wind_direction_rad()).toEqual(Math.PI);
    })
  })

});

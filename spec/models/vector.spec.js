import Vector from "../../src/models/vector.js";

describe("Vector", () => {
  describe('constuctor()', () => {
    it("should construct with x and y", () => {
      const vector = new Vector({x:1, y:2});
      expect(vector.x).toEqual(1);
      expect(vector.y).toEqual(2);
    });
    describe("with direction and distance", () => {
      it("should construct direction 0", () => {
        const vector = new Vector({dir: 0, dist: 1});
        expect(vector.x).toEqual(0);
        expect(vector.y).toEqual(1);
      });
      it("should construct direction 26.57", () => {
        const vector = new Vector({dir: 26.57, dist: Math.sqrt(5)});
        expect(vector.x).toBeCloseTo(1, 0);
        expect(vector.y).toBeCloseTo(2, 0);
      });
      it("should construct direction 90", () => {
        const vector = new Vector({dir: 90, dist: 1});
        expect(vector.x).toBeCloseTo(1, 0);
        expect(vector.y).toBeCloseTo(0, 0);
      });
      it("should construct direction 270", () => {
        const vector = new Vector({dir: 270, dist: 1});
        expect(vector.x).toBeCloseTo(-1, 0);
        expect(vector.y).toBeCloseTo(0, 0);
      });
      it("should construct direction 360", () => {
        const vector = new Vector({dir: 360, dist: 1});
        expect(vector.x).toBeCloseTo(0, 0);
        expect(vector.y).toBeCloseTo(1, 0);
      });
      it("should construct with any direction", () => {
        for (let dir = 0; dir <= 359; dir++) {
          const vector = new Vector({dir: dir, dist: dir + 1});
          expect(vector.direction()).toBeCloseTo(dir, 0);
          expect(vector.distance()).toBeCloseTo((dir + 1), 0);
        }
      });
    });
  });

  describe('direction()', () => {
    it("should return 0 correctly", () => { expect(new Vector({x:0, y:1}).direction()).toEqual(0) });
    it("should return 26.57 correctly", () => { expect(new Vector({x:1, y:2}).direction()).toBeCloseTo(26.57) });
    it("should return 45 correctly", () => { expect(new Vector({x:1, y:1}).direction()).toEqual(45); });
    it("should return 90 correctly", () => { expect(new Vector({x:1, y:0}).direction()).toEqual(90); });
    it("should return 135 correctly", () => { expect(new Vector({x:1, y:-1}).direction()).toEqual(135) });
    it("should return 180 correctly", () => { expect(new Vector({x:0, y:-1}).direction()).toEqual(180) });
    it("should return 225 correctly", () => { expect(new Vector({x:-1, y:-1}).direction()).toEqual(225) });
    it("should return 270 correctly", () => { expect(new Vector({x:-1, y:0}).direction()).toEqual(270) });
    it("should return 315 correctly", () => { expect(new Vector({x:-1, y:1}).direction()).toEqual(315) });
    it("should return 360 correctly", () => { expect(new Vector({x:0, y:1}).direction()).toEqual(0) });
  });

  describe('distance()', () => {
    it("should return distance correctly", () => {
      expect(new Vector({x:3, y:4}).distance()).toEqual(5)
    });
  });

  describe('add()', () => {
    it("should add correctly", () => {
      expect(new Vector({x:1, y:0}).add(new Vector({x:0, y:1}))).toEqual(new Vector({x:1, y:1}));
      expect(new Vector({x:0, y:0}).add(new Vector({x:-1, y:-1}))).toEqual(new Vector({x:-1, y:-1}));
    });
  });

  describe('mul()', () => {
    it("should multiply correctly", () => {
      expect(new Vector({x:1, y:0}).mul(3)).toEqual(new Vector({x:3, y:0}));
      expect(new Vector({x:-1, y:-2}).mul(2)).toEqual(new Vector({x:-2, y:-4}));
    });
  });

  describe('add()', () => {
    it("should substract correctly", () => {
      expect(new Vector({x:1, y:0}).sub(new Vector({x:0, y:1}))).toEqual(new Vector({x:1, y:-1}));
      expect(new Vector({x:0, y:0}).sub(new Vector({x:-1, y:-1}))).toEqual(new Vector({x:1, y:1}));
    });
  });
});

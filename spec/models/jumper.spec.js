import Jumper from "../../src/models/jumper.js";
import Level from "../../src/models/level.js";
import Jumprun from "../../src/models/jumprun.js";
import Vector from '../../src/models/vector.js'

describe("Jumper", () => {
  describe('constuctor()', () => {
    it("should construct without parameters", () => {
      const jumper = new Jumper();
      expect(jumper.id).not.toBeDefined();
    });
    it("should construct with parameters", () => {
      const jumper = new Jumper({
        name: 'Keke Rosberg',
        openAlt: 1500,
        canopyDescRate: 5,
        canopyGlideAngle: 3,
        setUpAlt: 200,
        color: '#c3c3c3',
        terminalVelocity: 0
      });
      expect(jumper.name).toEqual('Keke Rosberg');
      expect(jumper.terminalVelocity).toEqual(0);
    });
  });

  describe('optimumSpot()', () => {
    const jumper = new Jumper({
      openAlt: 1500,
      canopyDescRate: 5,
      setUpAlt: 0,
      terminalVelocity: 50
    });
    describe('when jumprun speed is 0', () => {
      const jumprun = new Jumprun({height_m: 4000, ias_ms: 0});
      it('should return 0 vector when there is no wind', () => {
        const levels = [new Level({height_m: 1000, wind_direction_deg: 240, wind_speed_ms: 0})];
        expect(jumper.optimumSpot(levels, jumprun, 0)).toEqual(new Vector({x:0, y:0}))
      });
      it('should return same direction as wind direction, when there is oly one level', () => {
        for (let dir = 0; dir <= 359; dir++) {
          const levels = [new Level({height_m: 1000, wind_direction_deg: dir, wind_speed_ms: 10})];
          expect(jumper.optimumSpot(levels, jumprun, 0).direction()).toBeCloseTo(dir);
        }
      });
      it('should return correct distance for a hop n pop', () => {
        const levels = [new Level({height_m: 1000, wind_direction_deg: 240, wind_speed_ms: 1})];
        jumper.openAlt = 4000;
        expect(jumper.optimumSpot(levels, jumprun, 0).distance()).toBeCloseTo(800);
      });
      it('should return correct distance for a once in a lifetime jump', () => {
        const levels = [new Level({height_m: 1000, wind_direction_deg: 240, wind_speed_ms: 1})];
        jumper.openAlt = 0;
        expect(jumper.optimumSpot(levels, jumprun, 0).distance()).toBeCloseTo(83.5, 0);
      });
      describe('jumper has a setup altitude', () => {
        it('should ', () => {

        })
      })
    });
    describe('when jumprun speed is greater than 0', () => {
      const levels = [new Level({height_m: 1000, wind_direction_deg: 0, wind_speed_ms: 0})];
      it('should return opposite of jumprun direction when there is no wind', () => {
        for (let dir = 0; dir <= 359; dir++) {
          const jumprun = new Jumprun({height_m: 4000, ias_ms: 30});
          expect(jumper.optimumSpot(levels, jumprun, dir).direction()).toBeCloseTo((dir + 180) % 360);
          expect(jumper.optimumSpot(levels, jumprun, dir).distance()).toBeCloseTo(236, 0);
        }
      });
    });
  });

  describe('timeSpentInSegment()', () => {
    const segment = { bottom: 1000, top: 2000 };
    const jumprun = new Jumprun({height_m: 4000, ias_ms: 30});
    const jumper = new Jumper({terminalVelocity: 50, canopyDescRate: 5});
    it('only freefall', () => {
      jumper.openAlt = 900;
      expect(jumper.timeSpentInSegment(segment, jumprun)).toBeCloseTo(20);
    });
    it('only canopy', () => {
      jumper.openAlt = 2100;
      expect(jumper.timeSpentInSegment(segment, jumprun)).toEqual(200);
    });
    it('opening in segment', () => {
      jumper.openAlt = 1500;
      expect(jumper.timeSpentInSegment(segment, jumprun)).toBeCloseTo(110);
    });
    it('hop n pop', () => {
      segment.bottom = 1981;
      jumprun.height_m = 2000;
      jumper.openAlt = 1981;
      expect(jumper.timeSpentInSegment(segment, jumprun)).toBeCloseTo(2, 0);
    });
  });

  describe('throwDistance()', () => {
    const jumprun = new Jumprun({height_m: 4000, ias_ms: 30});
    it('should return 0 when there is no freefall', () => {
      const jumper = new Jumper({openAlt: 4000, terminalVelocity: 50});
      expect(jumper.throwDistance(jumprun)).toEqual(0);
    });
    it('should return something', () => {
      const jumper = new Jumper({openAlt: 3800, terminalVelocity: 50});
      expect(jumper.throwDistance(jumprun)).toBeCloseTo(132, 0);
    });
    it('seems to return something else', () => {
      const jumper = new Jumper({openAlt: 1500, terminalVelocity: 50});
      expect(jumper.throwDistance(jumprun)).toBeCloseTo(226, 0);
    });
    it('seems to return yet something else', () => {
      const jumper = new Jumper({openAlt: 1500, terminalVelocity: 70});
      expect(jumper.throwDistance(jumprun)).toBeCloseTo(377, 0);
    });
  });

  describe('timeSpentInSegmentFreefall()', () => {
    const jumper = new Jumper({ terminalVelocity: 50 });
    describe('acceleration', () => {
      /*
       * Comparison to: http://www.offheading.com/tabels.html
       */
      it("should return 1 second when segment is 4.8 meter", () => {
        expect(jumper.timeSpentInSegmentFreefall({bottom: 0, top: 4.8}, {height_m: 4.8})).toBeCloseTo(1,0);
      });
      it("should return 2 seconds when segment is 19 meter", () => {
        expect(jumper.timeSpentInSegmentFreefall({bottom: 0, top: 19}, {height_m: 19})).toBeCloseTo(2,0);
      });
      it("should return 3 seconds when segment is 42 meter", () => {
        expect(jumper.timeSpentInSegmentFreefall({bottom: 0, top: 42}, {height_m: 42})).toBeCloseTo(3,0);
      });
      it("should return 4 seconds when segment is 74 meter", () => {
        expect(jumper.timeSpentInSegmentFreefall({bottom: 0, top: 74}, {height_m: 74})).toBeCloseTo(4,0);
      });
      it("should return 5 seconds when segment is 111 meter", () => {
        expect(jumper.timeSpentInSegmentFreefall({bottom: 0, top: 111}, {height_m: 111})).toBeCloseTo(5,0);
      });
      it("should return 6 seconds when segment is 153 meter", () => {
        expect(jumper.timeSpentInSegmentFreefall({bottom: 0, top: 153}, {height_m: 153})).toBeCloseTo(6,0);
      });
      it("should return 7 seconds when segment is 198 meter", () => {
        expect(jumper.timeSpentInSegmentFreefall({bottom: 0, top: 198}, {height_m: 198})).toBeCloseTo(7,0);
      });
      it("should return 8 seconds when segment is 246 meter", () => {
        expect(jumper.timeSpentInSegmentFreefall({bottom: 0, top: 246}, {height_m: 246})).toBeCloseTo(8,0);
      });
    });

    describe('steady speed segment', () => {
      it('should return 1 second for 50m segment', () => {
        expect(jumper.timeSpentInSegmentFreefall({bottom: 0, top: 50}, {height_m: 10000})).toEqual(1);
      })
    });
  });

  describe('timeSpentInSegmentCanopy()', () => {
    const jumper = new Jumper({ canopyDescRate: 5 });
    it('should return 1 second for 5m segment', () => {
      expect(jumper.timeSpentInSegmentCanopy({bottom: 0, top: 5})).toEqual(1);
    })
  });

  describe('circleRadius()', () => {
    const jumper = new Jumper({canopyGlideAngle: 2, openAlt: 1000, setUpAlt: 0});
    describe('when jump height > openAlt', () => {
      it('should return glide angle times openAlt - setUpAlt', () => {
        expect(jumper.circleRadius({height_m: 4000})).toEqual(2000);
      })
    });
    describe('when jump height < openAlt', () => {
      it('should return glide angle times openAlt - setUpAlt', () => {
        expect(jumper.circleRadius({height_m: 900})).toEqual(1800);
      })
    })
  });

  describe('segments()', () => {
    const jumper = new Jumper({setUpAlt: 0});
    const jumprun = {height_m: 4000};
    const level1 = new Level({height_m: 0, wind_speed_ms: 0, wind_direction_deg: 0});
    const level2 = new Level({height_m: 1000, wind_speed_ms: 10, wind_direction_deg: 100});
    const level3 = new Level({height_m: 3000, wind_speed_ms: 30, wind_direction_deg: 300});

    describe('when parameter levels is []',() => {
      it('should return []', () => {
        expect(jumper.segments([], jumprun)).toEqual([]);
      })
    });

    describe('when there is one level',() => {
      it('should return one segment', () => {
        expect(jumper.segments([level1], jumprun)).toEqual([{
          top: 4000, bottom: 0, wind_speed_ms: 0, wind_direction_deg: 0
        }]);
      });
      it('should return one segment correctly if level height is greater than jump height', () => {
        expect(jumper.segments([level3], {height_m: 2000})).toEqual([{
          top: 2000, bottom: 0, wind_speed_ms: 30, wind_direction_deg: 300
        }]);
      })
    });

    describe('when there are two levels',() => {
      describe('when jump height is lower than average of the levels',() => {
        it('should return one segment', () => {
          expect(jumper.segments([level3, level1], {height_m: 1000})).toEqual([
            {top: 1000, bottom: 0, wind_speed_ms: 0, wind_direction_deg: 0}
          ]);
        });
      });
      describe('when jump height is higher than average of the levels',() => {
        it('should return two segments', () => {
          expect(jumper.segments([level3, level1], {height_m: 2000})).toEqual([
            {top: 2000, bottom: 1500, wind_speed_ms: 30, wind_direction_deg: 300},
            {top: 1500, bottom: 0, wind_speed_ms: 0, wind_direction_deg: 0}
          ]);
        });
        it('should return two segments if jump height is higher than higher segment height', () => {
          expect(jumper.segments([level3, level1], {height_m: 4000})).toEqual([
            {top: 4000, bottom: 1500, wind_speed_ms: 30, wind_direction_deg:300},
            {top: 1500, bottom: 0, wind_speed_ms: 0, wind_direction_deg: 0}
          ]);
        });
      });
    });

    describe('when there are three levels including ground level',() => {
      const levels = [level1, level3, level2];
      it('should return 3 segments when jump height is greater than highest level', () => {
        expect(jumper.segments(levels, jumprun)).toEqual([
          {top: 4000, bottom: 2000, wind_speed_ms: 30, wind_direction_deg: 300},
          {top: 2000, bottom: 500, wind_speed_ms: 10, wind_direction_deg: 100},
          {top: 500, bottom: 0, wind_speed_ms: 0, wind_direction_deg: 0}
        ])
      });
      it('should return 3 segments when jump height equal to highest level', () => {
        expect(jumper.segments(levels, {height_m: 3000})).toEqual([
          {top: 3000, bottom: 2000, wind_speed_ms: 30, wind_direction_deg: 300},
          {top: 2000, bottom: 500, wind_speed_ms: 10, wind_direction_deg: 100},
          {top: 500, bottom: 0, wind_speed_ms: 0, wind_direction_deg: 0}
        ])
      });
      it('should return 3 segments when jump height between highest breakpoint and highest level', () => {
        expect(jumper.segments(levels, {height_m: 2500})).toEqual([
          {top: 2500, bottom: 2000, wind_speed_ms: 30, wind_direction_deg: 300},
          {top: 2000, bottom: 500, wind_speed_ms: 10, wind_direction_deg: 100},
          {top: 500, bottom: 0, wind_speed_ms: 0, wind_direction_deg: 0}
        ])
      });
      it('should return 2 segments when jump height equal to highest breakpoint', () => {
        expect(jumper.segments(levels, {height_m: 2000})).toEqual([
          {top: 2000, bottom: 500, wind_speed_ms: 10, wind_direction_deg: 100},
          {top: 500, bottom: 0, wind_speed_ms: 0, wind_direction_deg: 0}
        ])
      });
      it('should return 2 segments when jump height is between middle level and highest breakpoint', () => {
        const levels = [level1, level3, level2];
        expect(jumper.segments(levels, {height_m: 1500})).toEqual([
          {top: 1500, bottom: 500, wind_speed_ms: 10, wind_direction_deg: 100},
          {top: 500, bottom: 0, wind_speed_ms: 0, wind_direction_deg: 0}
        ])
      });
      it('should return 2 segments when jump height is between first breakpoint and second level', () => {
        expect(jumper.segments(levels, {height_m: 900})).toEqual([
          {top: 900, bottom: 500, wind_speed_ms: 10, wind_direction_deg: 100},
          {top: 500, bottom: 0, wind_speed_ms: 0, wind_direction_deg: 0}
        ])
      });
      it('should return 1 segment when jump height equal to first breakpoint', () => {
        expect(jumper.segments(levels, {height_m: 500})).toEqual([
          {top: 500, bottom: 0, wind_speed_ms: 0, wind_direction_deg: 0}
        ])
      });
      it('should return 1 segment when jump height is under first breakpoint', () => {
        expect(jumper.segments(levels, {height_m: 400})).toEqual([
          {top: 400, bottom: 0, wind_speed_ms: 0, wind_direction_deg: 0}
        ])
      })
    });
    describe('when jumper has a set up altitude', () => {
      const swooper = new Jumper({setUpAlt: 200});
      it('the lowest segment should have setUpAlt as bottom', () => {
        expect(swooper.segments([level1], jumprun)).toEqual([{
          top: 4000, bottom: 200, wind_speed_ms: 0, wind_direction_deg: 0
        }]);
      });
      it('the lowest segment should have setUpAlt as bottom', () => {
        expect(swooper.segments([level1, level3, level2], jumprun)).toEqual([
          {top: 4000, bottom: 2000, wind_speed_ms: 30, wind_direction_deg: 300},
          {top: 2000, bottom: 500, wind_speed_ms: 10, wind_direction_deg: 100},
          {top: 500, bottom: 200, wind_speed_ms: 0, wind_direction_deg: 0}
        ]);
      });
      it('should return two out of three segements if setup altitude is > lowest segent top', () => {
        expect(new Jumper({setUpAlt: 600}).segments([level1, level3, level2], jumprun)).toEqual([
          {top: 4000, bottom: 2000, wind_speed_ms: 30, wind_direction_deg: 300},
          {top: 2000, bottom: 600, wind_speed_ms: 10, wind_direction_deg: 100}
        ]);
      });
    });
  });
});

import Hello from "../../src/models/hello_world";

describe("Hello", () => {
  it("greets the user", () => {
    let hello = new Hello();
    expect(hello.greet()).toBe("Hello world!");
  });
});

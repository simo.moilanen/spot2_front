import GpsWindMeter from "../../src/models/gps_wind_meter/gps_wind_meter.js";
import Segment from "../../src/models/gps_wind_meter/segment.js";

describe("GpsWindMeter", () => {
  describe('constuctor()', () => {
    const gps_wind_meter = new GpsWindMeter();
    it("creates n segments", () => {
      expect(gps_wind_meter.segments.length).toEqual(8);
    });
  });
  describe('geoSuccess()', () => {
    it("adds a reading to a segment", () => {
      const gps_wind_meter = new GpsWindMeter();
      const event = {coords: {altitude: 100, heading: 240, speed: 55}};
      gps_wind_meter.geoSuccess(event);
      expect(gps_wind_meter.segments[0].data.length).toEqual(1);
    });
    it("sets a segment active", () => {
      const gps_wind_meter = new GpsWindMeter();
      const event = {coords: {altitude: 100, heading: 240, speed: 55}};
      expect(gps_wind_meter.segments[0].active).toEqual(false);
      gps_wind_meter.geoSuccess(event);
      expect(gps_wind_meter.segments[0].active).toEqual(true);
    });
  });
});

describe("Segment", () => {
  describe('constuctor()', () => {
    const segment = new Segment(300, 600);
    it("constructs object", () => {
      expect(segment.top).toEqual(600);
    });
  });
  describe('addReading()', () => {
    const segment = new Segment(300, 600);
    it("adds a vector to data", () => {
      segment.addReading(240.0, 50.0);
      expect(segment.data.length).toEqual(1);
    });
  });
  describe('wind()', () => {
    const segment = new Segment(300, 600);
    segment.addReading(90.0, 55.0);
    segment.addReading(270.0, 45.0);
    segment.addReading(180.0, 50.0);
    const wind = segment.wind();
    it("returns correct direction", () => {
      expect(wind.direction()).toBeCloseTo(273.0, 0);
    });
    it("returns correct speed", () => {
      expect(wind.distance()).toBeCloseTo(5.0, 0);
    });
  });
});
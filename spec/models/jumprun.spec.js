import Jumprun from "../../src/models/jumprun.js";
import Level from "../../src/models/level.js";

describe("Jumprun", () => {
  describe('constuctor()', () => {
    it("should construct with SI units", () => {
      const jumprun = new Jumprun({height_m: 4000, ias_ms: 30});
      expect(jumprun.height_m).toEqual(4000);
      expect(jumprun.ias_ms).toEqual(30);
    });
    it("should construct with imperial units", () => {
      const jumprun = new Jumprun({height_ft: 4000, ias_kt: 30});
      expect(jumprun.height_m).toEqual(1219.2);
      expect(jumprun.ias_ms).toBeCloseTo(15.4333, 2);
    });
  });
  
  describe('tas_ms()', () => {
    it("should return greater than ias :D", () => {
      const jumprun = new Jumprun({height_m: 4000, ias_ms: 30});
      expect(jumprun.tas_ms()).toBeCloseTo(38, 0);
    });
    it("seems to not return same as ias at sea level", () => {
      const jumprun = new Jumprun({height_m: 0, ias_ms: 30});
      expect(jumprun.tas_ms()).toBeCloseTo(31, 0);
    });
  });

  describe('tas_kt()', () => {
    it("should return greater than ias :D", () => {
      const jumprun = new Jumprun({height_m: 4000, ias_kt: 30});
      expect(jumprun.tas_kt()).toBeCloseTo(38, 0);
    });
  });

  describe('jumpRunLevel()', () => {
    const level1 = new Level({height_m: 1000, wind_speed_ms: 10, wind_direction_deg: 300});
    const level2 = new Level({height_m: 2000, wind_speed_ms: 10, wind_direction_deg: 300});
    const level3 = new Level({height_m: 3000, wind_speed_ms: 10, wind_direction_deg: 300});
    it("should return the level affecting at jumprun height", () => {
      const jumprun = new Jumprun({height_m: 4000, ias_kt: 30});
      expect(jumprun.jumpRunLevel([level1, level2, level3]).height_m).toEqual(3000)
    });
    it("should return the level affecting at jumprun height", () => {
      const jumprun = new Jumprun({height_m: 2501, ias_kt: 30});
      expect(jumprun.jumpRunLevel([level1, level2, level3]).height_m).toEqual(3000)
    });
    it("should return the level affecting at jumprun height", () => {
      const jumprun = new Jumprun({height_m: 2499, ias_kt: 30});
      expect(jumprun.jumpRunLevel([level1, level2, level3]).height_m).toEqual(2000)
    });
    it("should return the level affecting at jumprun height", () => {
      const jumprun = new Jumprun({height_m: 1499, ias_kt: 30});
      expect(jumprun.jumpRunLevel([level1, level2, level3]).height_m).toEqual(1000)
    });
    it("should return the level affecting at jumprun height", () => {
      const jumprun = new Jumprun({height_m: 1, ias_kt: 30});
      expect(jumprun.jumpRunLevel([level1, level2, level3]).height_m).toEqual(1000)
    });
  });

  describe('groundSpeed()', () => {
    describe('no wind', () => {
      const level = new Level({height_m: 4000, wind_speed_ms: 0, wind_direction_deg: 300});
      it("should return jumprun tas", () => {
        const jumprun = new Jumprun({height_m: 4000, ias_kt: 70});
        expect(jumprun.groundSpeed_ms([level], 300)).toEqual(jumprun.tas_ms());
      });
    });
    describe('headwind', () => {
      const level = new Level({height_m: 4000, wind_speed_ms: 10, wind_direction_deg: 300});
      it("should return jumprun tas - wind", () => {
        const jumprun = new Jumprun({height_m: 4000, ias_kt: 70});
        const expected = jumprun.tas_ms() - level.wind_speed_ms;
        expect(jumprun.groundSpeed_ms([level], 300)).toBeCloseTo(expected);
      });
    });
    describe('tailwind', () => {
      const level = new Level({height_m: 4000, wind_speed_ms: 10, wind_direction_deg: 0});
      it("should return jumprun tas + wind", () => {
        const jumprun = new Jumprun({height_m: 4000, ias_kt: 70});
        const expected = jumprun.tas_ms() + level.wind_speed_ms;
        expect(jumprun.groundSpeed_ms([level], 180)).toEqual(expected);
      });
    });
    describe('crosswind', () => {
      const level = new Level({height_m: 4000, wind_speed_ms: 10, wind_direction_deg: 180});
      it("should return a bit less thas jumprun tas", () => {
        const jumprun = new Jumprun({height_m: 4000, ias_kt: 70});
        const expected = jumprun.tas_ms() - 1;
        expect(jumprun.groundSpeed_ms([level], 90)).toBeCloseTo(expected, 0);
      });
    });
    describe('cross-head-wind', () => {
      const level = new Level({height_m: 4000, wind_speed_ms: 10, wind_direction_deg: 0});
      it("should return a bit less thas jumprun tas", () => {
        const jumprun = new Jumprun({height_m: 4000, ias_kt: 70});
        const expected = jumprun.tas_ms() - 8;
        expect(jumprun.groundSpeed_ms([level], 45)).toBeCloseTo(expected, 0);
      });
    });
  });
  describe('doorOpenDistance()', () => {
    const level = new Level({height_m: 4000, wind_speed_ms: 0, wind_direction_deg: 180});
    it("should return groundspeed * door_open_time", () => {
      const jumprun = new Jumprun({height_m: 4000, ias_ms: 30, door_open_time: 10});
      expect(jumprun.doorOpenDistance_m([level], 180)).toBeCloseTo(377, 0);
    });
  });
});

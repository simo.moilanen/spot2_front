import React from 'react';
import {shallow, mount} from 'enzyme';
import LevelsTable from '../src/react_components/levels_table.jsx';
import { Table } from 'semantic-ui-react';

describe('LevelsTable SHALLLOW', () => {
  const levelsTable = shallow(
    <LevelsTable
      levels={[{id: 1, height_ft: 0, wind_direction_deg: 240, wind_speed_kt: 12, temperature_c: 10},
               {id: 2, height_ft: 1000, wind_direction_deg: 250, wind_speed_kt: 14, temperature_c: 8}]}
    />
  );

  it('should render one Table', () => {
    expect(levelsTable.find(Table).length).toEqual(1)
  })
});

describe('LevelsTable MOUNT', () => {
  const levelsTable = mount(
    <LevelsTable
      levels={[{id: 1, height_ft: 0, wind_direction_deg: 240, wind_speed_kt: 12, temperature_c: 10},
        {id: 2, height_ft: 1000, wind_direction_deg: 250, wind_speed_kt: 14, temperature_c: 8}]}
    />
  );

  it('should render two rows given two levels', () => {
    expect(levelsTable.find('tbody tr').length).toEqual(2)
  })
});

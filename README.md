# Spot2 frontend

* `npm install`
* `npm start` > http://localhost:8080/
* `npm test`
* `npm run test -- --watch`

## Build to Rails project

`./build.sh`

## Make development server public:

* `./node_modules/.bin/webpack-dev-server --hot --inline --content-base dist/ --host 0.0.0.0`
* `rails s --binding=0.0.0.0`

import React from 'react'
import ReactDOM from 'react-dom'
import App from './react_components/app.jsx'

ReactDOM.render(
  <App />,
  document.getElementById('root')
);

import React from 'react';
import { Form } from 'semantic-ui-react'
import { InstructionPopup } from './instruction_popup.jsx'

export default class JumprunController extends React.Component {
  render() {
    return (
      <Form>
        <Form.Input
          name="height_m"
          label="Jump height (m)"
          value={this.props.jumprun.height_m}
          onChange={this.props.onJumprunChange}
          type="number"
          pattern="\d*"
          min="0"
          step="100"
        />
        <Form.Field>
          <label>
            Jumprun speed (kt)
            <InstructionPopup
              header='Jumprun speed'
              content='The indicated airspeed of the airplane on jumprun.'
            />
          </label>
          <input
            name="ias_kt"
            value={this.props.jumprun.ias_kt()}
            onChange={this.props.onJumprunChange}
            type="number"
            pattern="\d*"
            min="0"
            step="1"
          />
          </Form.Field>
        <Form.Input
          name="door_open_time"
          label="Time from green light to first exit (seconds)"
          value={this.props.jumprun.door_open_time}
          onChange={this.props.onJumprunChange}
          type="number"
          pattern="\d*"
          min="0"
          step="1"
        />
        <Form.Input
          name="groups"
          label="Number of groups"
          value={this.props.jumprun.groups}
          onChange={this.props.onJumprunChange}
          type="number"
          pattern="\d*"
          min="0"
          step="1"
        />
        <Form.Input
          name="group_separation_time"
          label="Seconds between groups"
          value={this.props.jumprun.group_separation_time}
          onChange={this.props.onJumprunChange}
          type="number"
          pattern="\d*"
          min="0"
          step="1"
        />
        <Form.Field>
          <label>Favoured jumper</label>
          <select
            name="favoured_jumper_id"
            value={this.props.jumprun.favoured_jumper_id}
            onChange={this.props.onJumprunChange}
          >
            {this.props.jumpers.map((jumper) =>
              <option key={jumper.id} value={jumper.id}>{jumper.name}</option>
            )}
          </select>
        </Form.Field>
        <Form.Field>
          <label>Jumprun type</label>
          <Form.Radio label='"Headwind" (Over the top of dz towards favoured jumper optimum spot)'
                      name='type'
                      value='headwind'
                      checked={this.props.jumprun.type === 'headwind'}
                      onChange={this.props.onJumprunChange}/>
          <Form.Radio label='Parallel to runway'
                      name='type'
                      value='runway'
                      checked={this.props.jumprun.type === 'runway'}
                      onChange={this.props.onJumprunChange} />
        </Form.Field>
      </Form>
    );
  }
}

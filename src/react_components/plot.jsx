import React, { Component } from 'react';
//import Plotly from 'plotly.js';

export default class Plot extends Component {

  render() {
    return (
      <div ref='plotDiv'></div>
    )
  }

  componentDidMount() {
    this.draw();
  }

  componentDidUpdate(prevProps) {
    this.draw();
  }

  draw() {
    const layout = {
      margin: {
        l: 0,
        r: 0,
        b: 0,
        t: 0,
        pad: 0
      },
      paper_bgcolor: '#7f7f7f',
      plot_bgcolor: '#c7c7c7'
    };
    const trace1 = {
      x: this.props.segment.data.map((vector) => vector.x),
      y: this.props.segment.data.map((vector) => vector.y),
      mode: 'markers',
      type: 'scatter'
    };
    Plotly.newPlot(this.refs.plotDiv, [trace1], layout);
  }
}

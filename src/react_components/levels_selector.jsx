import React from 'react';
import { Menu, Table } from 'semantic-ui-react'
import CustomLevelsForm from './custom_levels_form.jsx'
import LevelsTable from './levels_table.jsx'

export default class LevelsSelector extends React.Component {
  render() {
    return (
      <div>
        <Menu fluid vertical className="levels-selector">
          <Menu.Item
            name="jeppesen"
            active={this.props.selectedLevelsType === "jeppesen"}
            onClick={() => this.props.onSetLevelsType('jeppesen')}
          />
          <Menu.Item
            name="fmi"
            active={this.props.selectedLevelsType === "fmi"}
            onClick={() => this.props.onSetLevelsType('fmi')}
          />
          <Menu.Item
            className="custom-levels-select"
            name="custom"
            active={this.props.selectedLevelsType === "custom"}
            onClick={() => this.props.onSetLevelsType('custom')}
          />
        </Menu>
        { this.props.selectedLevelsType === "custom" &&
        <CustomLevelsForm 
          customLevels={this.props.customLevels}
          onCustomLevelInputChange={this.props.onCustomLevelInputChange}
          removeCustomLevelRow={this.props.removeCustomLevelRow}
        /> }
        { ['jeppesen', 'fmi'].includes(this.props.selectedLevelsType) &&
          <LevelsTable
            levels={this.props.levels}
          />}
      </div>
    );
  }
}

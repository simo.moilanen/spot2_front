import React from 'react';
import { Icon, Popup } from 'semantic-ui-react'

export function InstructionPopup(props) {
  return (
    <Popup
      trigger={<Icon name="question" color="blue" />}
      header={props.header}
      content={props.content}
    />
  )
}

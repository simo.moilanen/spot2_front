import React from 'react'
import MenuTabs from './menu_tabs.jsx'
import Map from './map.jsx'
import superagent from 'superagent';
import Level from '../models/level.js';
import Jumper from '../models/jumper.js';
import Jumprun from '../models/jumprun';
import MapLegend from './map_legend.jsx';
import { Dimmer, Loader, Sidebar, Icon, Button, Checkbox } from 'semantic-ui-react'

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dropzones: [],
      selectedDropzone: {"id":1,"name":"Malmi","icao":"EFHF","lat":60.252271,"lng":25.039227,"runway_direction":186},
      levels: [],
      customLevels: [new Level({id: 1})],
      selectedLevelsType: 'jeppesen',
      jumpers: [new Jumper({
        id: 1,
        name: 'Tandem',
        openAlt: 1200,
        canopyDescRate: 4,
        canopyGlideAngle: 2,
        setUpAlt: 100,
        color: '#FFFF00',
        terminalVelocity: 50
      })],
      jumprun: new Jumprun({height_m: 3000, ias_kt: 70, favoured_jumper_id: 1, door_open_time: 20, group_separation_time: 10, groups: 2, type: 'headwind'}),
      loadingStack: 0,
      menuVisible: true,
      gpsTracking: false
    };
    this.setDropzone = this.setDropzone.bind(this);
    this.setLevelsType = this.setLevelsType.bind(this);
    this.onCustomLevelInputChange = this.onCustomLevelInputChange.bind(this);
    this.addJumper = this.addJumper.bind(this);
    this.updateJumper = this.updateJumper.bind(this);
    this.updateJumprun = this.updateJumprun.bind(this);
    this.setStateLs = this.setStateLs.bind(this);
    this.initializeStateFromLocalStorage = this.initializeStateFromLocalStorage.bind(this);
    this.jumprunDirection = this.jumprunDirection.bind(this);
    this.loadingStack = this.loadingStack.bind(this);
    this.toggleMenuVisibility = this.toggleMenuVisibility.bind(this);
    this.removeCustomLevelRow = this.removeCustomLevelRow.bind(this);
    this.removeJumper = this.removeJumper.bind(this);
    this.toggleGpsTracking = this.toggleGpsTracking.bind(this);
  }

  componentDidMount() {
    this.initializeStateFromLocalStorage(() => {
      if (this.state.selectedDropzone && this.state.selectedDropzone.id) {
        this.fetchAndSetJeppesenLevels();
      }
    });
    const self = this;
    this.loadingStack('+');
    superagent.get(apiUrl + 'dropzones')
      .then(function(response) {
        self.setState({
          dropzones: response.body,
          loadingStack: self.state.loadingStack - 1
        });
      });
  }

  fetchAndSetJeppesenLevels() {
    const self = this;
    this.loadingStack('+');
    superagent.get(apiUrl + 'dropzones/' + this.state.selectedDropzone.id)
      .then(function(response) {
        self.setState({
          levels: response.body.jeppesen_datum.levels.map(l => new Level(l)),
          loadingStack: self.state.loadingStack - 1
        });
      });
  }

  fetchAndSetFmiLevels() {
    const self = this;
    this.loadingStack('+');
    const lat = this.state.selectedDropzone.lat;
    const lng = this.state.selectedDropzone.lng;
    superagent.get(`https://obscure-sierra-34331.herokuapp.com/?lat=${lat}&lng=${lng}`)
      .then(function(response) {
        self.setState({
          levels: response.body.map(l => new Level(l)),
          loadingStack: self.state.loadingStack - 1
        });
      });
  }

  setDropzone(dropzone) {
    this.setStateLs({
      selectedDropzone: dropzone
    }, () => this.setLevels());
  }

  setLevelsType(levelsType) {
    this.setState({
      selectedLevelsType: levelsType
    }, () => this.setLevels());
  }

  setLevels() {
    switch(this.state.selectedLevelsType) {
      case 'jeppesen':
        this.fetchAndSetJeppesenLevels();
        break;
      case 'fmi':
        this.fetchAndSetFmiLevels();
        break;
      case 'custom':
        this.setState({levels: this.state.customLevels.filter((l) => l.height_m && l.wind_direction_deg && l.wind_speed_ms)});
        break;
    }
  }

  onCustomLevelInputChange(id, attribute, value) {
    let levels = this.state.customLevels;
    levels.find((l) => l.id == id).updateAttribute(attribute, parseFloat(value));
    if (levels.every((l) => l.allValuesNumeric())) {
      const highestId = Math.max(...levels.map((l) => l.id));
      levels.push(new Level({id: (highestId + 1)}));
    }
    this.setState({levels: levels.filter((l) => l.allValuesNumeric())});
    this.setStateLs({customLevels: levels});
  }

  removeCustomLevelRow(id) {
    let levels = this.state.customLevels.filter((l) => l.id != id);
    if (levels.length === 0) { levels.push(new Level({id: 1})) }
    this.setState({levels: levels.filter((l) => l.allValuesNumeric())});
    this.setStateLs({customLevels: levels});
  }

  addJumper(){
    let highestId;
    if (this.state.jumpers.length == 0) {
      highestId = 0;
    } else {
      highestId = Math.max(...this.state.jumpers.map((l) => l.id));
    }
    this.setStateLs({jumpers: this.state.jumpers.concat(new Jumper({
      id: (highestId + 1),
      name: 'New jumper',
      openAlt: 800,
      canopyDescRate: 6,
      canopyGlideAngle: 3,
      setUpAlt: 100,
      color: '#00FF00',
      terminalVelocity: 56}))});
  }

  updateJumper(id, attribute, value) {
    let jumpers = this.state.jumpers;
    jumpers.find((j) => j.id == id).updateAttribute(attribute, value);
    this.setStateLs({jumpers: jumpers});
  }

  removeJumper(id) {
    let jumpers = this.state.jumpers;
    jumpers.splice(jumpers.findIndex((j) => j.id == id), 1);
    this.setStateLs({jumpers: jumpers});
  }
  
  updateJumprun(event, target) {
    let jumprun = this.state.jumprun;
    jumprun.updateAttribute(event.target.name || target.name, event.target.value || target.value);
    this.setStateLs({jumprun: jumprun});
  }

  setStateLs(newStateObject, callback=null) {
    this.setState(newStateObject, callback);
    let lsObject = {};
    localStorage.spot2state && (lsObject = JSON.parse(localStorage.spot2state));
    Object.assign(lsObject, newStateObject);
    localStorage.spot2state = JSON.stringify(lsObject);
  }

  initializeStateFromLocalStorage(callback=null) {
    if (localStorage.spot2state) {
      const lsObject = JSON.parse(localStorage.spot2state);
      lsObject.jumprun && (lsObject.jumprun = new Jumprun(lsObject.jumprun));
      lsObject.jumpers && (lsObject.jumpers = lsObject.jumpers.map((jumperDetails) => new Jumper(jumperDetails)));
      lsObject.customLevels && (lsObject.customLevels = lsObject.customLevels.map((levelDetails) => new Level(levelDetails)));
      this.setState(lsObject, callback);
    } else {callback()}
  }

  jumprunDirection() {
    if (this.state.levels.length < 1) { return undefined; }
    if (this.state.jumprun.type === 'headwind') {
      const favouredJumper = this.state.jumpers.find((j) => j.id == this.state.jumprun.favoured_jumper_id);
      const optimumSpotOfFavouredJumper = favouredJumper.optimumSpotWithoutThrow(this.state.levels, this.state.jumprun);
      return optimumSpotOfFavouredJumper.direction();
    } else if (this.state.jumprun.type === 'runway') {
      const runwayDirection = this.state.selectedDropzone.runway_direction;
      const jumpRunWindDirection = this.state.jumprun.jumpRunLevel(this.state.levels).wind_direction_deg;
      if (jumpRunWindDirection > (runwayDirection - 90) && jumpRunWindDirection < (runwayDirection + 90)) {
        return runwayDirection;
      } else {
        return (runwayDirection + 180) % 360;
      }
    }
  }

  loadingStack(d) {
    if (d == '+') { this.setState({loadingStack: this.state.loadingStack + 1}) }
    if (d == '-') { this.setState({loadingStack: this.state.loadingStack - 1}) }
  }

  toggleMenuVisibility() { this.setState({ menuVisible: !this.state.menuVisible }); }

  toggleGpsTracking() {
    this.setState({gpsTracking: !this.state.gpsTracking})
  }

  render() {
    return (
      <div className="app">
        {this.state.loadingStack > 0 && <Dimmer active><Loader size='massive'/></Dimmer>}
        <Sidebar.Pushable>
          <Sidebar animation='overlay' width='wide' visible={this.state.menuVisible} icon='labeled'>
            <MenuTabs
              dropzones={this.state.dropzones}
              selectedDropzone={this.state.selectedDropzone}
              onSelectDropzone={this.setDropzone}
              selectedLevelsType={this.state.selectedLevelsType}
              onSetLevelsType={this.setLevelsType}
              customLevels={this.state.customLevels}
              onCustomLevelInputChange={this.onCustomLevelInputChange}
              removeCustomLevelRow={this.removeCustomLevelRow}
              levels={this.state.levels}
              jumpers={this.state.jumpers}
              onAddJumper={this.addJumper}
              onJumperRemove={this.removeJumper}
              onJumperChange={this.updateJumper}
              jumprun={this.state.jumprun}
              onJumprunChange={this.updateJumprun}
              toggleMenuVisibility={this.toggleMenuVisibility}
            />
            <Checkbox checked={this.state.gpsTracking} label="Track location on map" onChange={this.toggleGpsTracking} />
          </Sidebar>
          <Button icon size='big' className="menu-toggle" onClick={this.toggleMenuVisibility}>
            <Icon name="bars" />
          </Button>

          <Sidebar.Pusher>
            {!this.props.withoutMap && <Map
              dropzone={this.state.selectedDropzone}
              dropzones={this.state.dropzones}
              jumpers={this.state.jumpers}
              levels={this.state.levels}
              jumprun={this.state.jumprun}
              jumprunDirection={this.jumprunDirection}
              trackGps={this.state.gpsTracking}
            />}
            <MapLegend
              jumprunDirection={this.jumprunDirection}
              jumpers={this.state.jumpers}
            />
          </Sidebar.Pusher>
        </Sidebar.Pushable>

      </div>
    );
  }
}


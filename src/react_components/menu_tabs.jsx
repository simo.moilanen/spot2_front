import React, { Component } from 'react'
import { Menu, Segment, Icon, Checkbox } from 'semantic-ui-react'
import DropzoneSelector from './dropzone_selector.jsx'
import LevelsSelector from './levels_selector.jsx'
import JumpersController from './jumpers_controller.jsx'
import JumprunController from "./jumprun_controller.jsx"

export default class MenuTabs extends Component {
  constructor(props) {
    super(props);
    this.state = { activeItem: 'dz' };
    this.handleItemClick = this.handleItemClick.bind(this);
  }

  handleItemClick(e, { name }) {
    this.setState({ activeItem: name })
  }

  render() {
    const { activeItem } = this.state;
    let activeSegment = null;
    switch(activeItem) {
      case 'dz':
        activeSegment = (
          <DropzoneSelector
            dropzones={this.props.dropzones}
            selectedDropzone={this.props.selectedDropzone}
            onSelectDropzone={this.props.onSelectDropzone}
          />
        );
        break;
      case 'winds':
        activeSegment = (
          <LevelsSelector
            onSetLevelsType={this.props.onSetLevelsType}
            selectedLevelsType={this.props.selectedLevelsType}
            customLevels={this.props.customLevels}
            onCustomLevelInputChange={this.props.onCustomLevelInputChange}
            removeCustomLevelRow={this.props.removeCustomLevelRow}
            levels={this.props.levels}
          />
        );
        break;
      case 'jumpers':
        activeSegment = (
          <JumpersController
            jumpers={this.props.jumpers}
            onAddJumper={this.props.onAddJumper}
            onJumperChange={this.props.onJumperChange}
            onJumperRemove={this.props.onJumperRemove}
          />
        );
        break;
      case 'jumprun':
        activeSegment=(
          <JumprunController
            jumprun={this.props.jumprun}
            onJumprunChange={this.props.onJumprunChange}
            jumpers={this.props.jumpers}
          />
        )
    }

    return (
      <div className="menu-wrapper">
        <Menu attached='top' tabular>
          <Menu.Item icon name='dz' active={activeItem === 'dz'} onClick={this.handleItemClick}>
            <Icon name='marker' size='big'/>
          </Menu.Item>
          <Menu.Item icon name='winds' active={activeItem === 'winds'} onClick={this.handleItemClick}>
            <Icon name="flag outline" size="big"/>
          </Menu.Item>
          <Menu.Item icon name='jumpers' active={activeItem === 'jumpers'} onClick={this.handleItemClick}>
            <Icon name="user" size="big"/>
          </Menu.Item>
          <Menu.Item icon name='jumprun' active={activeItem === 'jumprun'} onClick={this.handleItemClick}>
            <Icon name="plane" size="big"/>
          </Menu.Item>
          <Menu.Item icon name='hide_menu' position='right' onClick={this.props.toggleMenuVisibility}>
            <Icon name="bars" size="big"/>
          </Menu.Item>
        </Menu>
        <Segment attached='bottom'>
          {activeSegment}
        </Segment>

      </div>
    )
  }
}

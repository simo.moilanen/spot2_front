import React from 'react';

export default class Map extends React.Component {

  render() {
    return <div className='map-canvas' ref="mapCanvas"/>
  }

  componentDidMount() {
    let mapCenter;
    let zoom;
    if (this.props.dropzone.lat && this.props.dropzone.lng) {
      mapCenter = this.dz();
      zoom = 13;
    } else {
      mapCenter = {lat: 63.109111, lng: 25.521540};
      zoom = 6;
    }
    const mapOptions = {
      center: mapCenter,
      zoom: zoom,
      mapTypeId: google.maps.MapTypeId.SATELLITE,
      panControl: false,
      zoomControl: false,
      streetViewControl: false,
      mapTypeControl: false
    };
    this.map = new google.maps.Map(this.refs.mapCanvas, mapOptions);
    this.map.controls[google.maps.ControlPosition.RIGHT_TOP].push(document.getElementById('map-legend'));
    this.drawHelsinkiAtcBorders();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.dropzone !== this.props.dropzone) {
      this.map.setZoom(13);
      this.map.panTo(this.props.dropzone);
    }
    if (this.props.dropzones.length != prevProps.dropzones.length) {
      this.props.dropzones.forEach((dz) => {
        new google.maps.Marker({position: dz, map: this.map, title: dz.name, animation: google.maps.Animation.DROP});
      });
    }
    const requirementsForDrawingJumpersAndJumprunArePresent =
      this.props.dropzone.lat && this.props.dropzone.lng && this.props.jumprun && this.props.levels.length > 0;
    if (requirementsForDrawingJumpersAndJumprunArePresent) {
      this.drawJumpers();
      this.drawGroups();
      this.drawRulers();
    }
    if (this.props.trackGps != prevProps.trackGps) {
      if (this.props.trackGps) {
        this.startGpsTracking();
      } else {
        this.stopGpsTracking();
      }
    }
  }

  drawJumpers() {
    this.jumperCircles && this.jumperCircles.forEach((circle) => { circle.setMap(null); });
    this.jumperCircles = [];
    this.props.jumpers.forEach((jumper) => {
      const optimumSpot = jumper.optimumSpot(this.props.levels, this.props.jumprun, this.props.jumprunDirection());
      this.jumperCircles.push(new google.maps.Circle({
        strokeColor: jumper.color,
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillOpacity: 0,
        map: this.map,
        center: google.maps.geometry.spherical.computeOffset(this.dz(), optimumSpot.distance(), optimumSpot.direction()),
        radius: jumper.circleRadius(this.props.jumprun)
      }));
    })
  }

  drawGroups() {
    this.polylineGroups && this.polylineGroups.setMap(null);
    const doorOpenDist = this.props.jumprun.doorOpenDistance_m(this.props.levels, this.props.jumprunDirection());
    const groupDist = this.props.jumprun.groupDistance_m(this.props.levels, this.props.jumprunDirection());
    const polyLineLength = doorOpenDist + groupDist * (this.props.jumprun.groups - 1);
    const favouredJumper = this.props.jumpers.find((j) => j.id == this.props.jumprun.favoured_jumper_id);
    const optimumSpotOfFavouredJumper = favouredJumper.optimumSpot(this.props.levels, this.props.jumprun, this.props.jumprunDirection());
    const optimumSpot = google.maps.geometry.spherical.computeOffset(this.dz(), optimumSpotOfFavouredJumper.distance(), optimumSpotOfFavouredJumper.direction());
    const distanceFromOptimumSpotToBeginnigPoint =  (groupDist * (this.props.jumprun.groups - 1))/2 + doorOpenDist;
    const beginningPoint = google.maps.geometry.spherical.computeOffset(optimumSpot, -distanceFromOptimumSpotToBeginnigPoint, this.props.jumprunDirection());
    const groupIcon = {
      path: google.maps.SymbolPath.CIRCLE,
      fillColor: '#c3c3c3',
      strokeColor: '#000000',
      fillOpacity: 1,
      strokeWeight: 1
    };
    const greenLightIcon = {
      path: google.maps.SymbolPath.CIRCLE,
      fillColor: '#00FF00',
      strokeColor: '#000000',
      fillOpacity: 1,
      strokeWeight: 1
    };
    let groupIcons = [{icon: greenLightIcon, offset: '0%'}];
    for (let i=0; i < this.props.jumprun.groups; i++) {
      groupIcons.push({
        icon: groupIcon,
        offset: ((doorOpenDist + groupDist * i) / polyLineLength) * 100 + '%'
      })
    }
    this.polylineGroups = new google.maps.Polyline({
      path: [
        beginningPoint,
        google.maps.geometry.spherical.computeOffset(beginningPoint, polyLineLength, this.props.jumprunDirection())
      ],
      icons: groupIcons,
      strokeColor: 'transparent',
      map: this.map,
      strokeWeight: 8,
      zIndex: 2,
      draggable: true
    })
  }

  drawRulers() {
    this.rulers && this.rulers.forEach((ruler) => { ruler.setMap(null); });
    this.rulers = [];
    if (this.props.jumprun.type === 'headwind') {
      this.rulers.push(this.ruler(this.dz()));
    } else if (this.props.jumprun.type === 'runway') {
      const parallelJumprunSeparation = 200; // TODO backend
      [...Array(11).keys()].forEach((i) => {
        const center = google.maps.geometry.spherical.computeOffset(this.dz(), parallelJumprunSeparation * i, this.props.jumprunDirection() + 90);
        this.rulers.push(this.paralleToJumprunLine(center));
        const center2 = google.maps.geometry.spherical.computeOffset(this.dz(), parallelJumprunSeparation * i, this.props.jumprunDirection() - 90);
        this.rulers.push(this.paralleToJumprunLine(center2));
      })
    }
  }

  paralleToJumprunLine(center) {
    const jumpRunEnds = [
      google.maps.geometry.spherical.computeOffset(center, -3 * 1852, this.props.jumprunDirection()),
      google.maps.geometry.spherical.computeOffset(center, 3 * 1852, this.props.jumprunDirection())
    ];
    return new google.maps.Polyline({
      path: jumpRunEnds,
      strokeColor: '#ffffff',
      strokeOpacity: 1.0,
      strokeWeight: 1,
      map: this.map
    });
  }

  ruler(center) {
    const jumpRunEnds = [
      google.maps.geometry.spherical.computeOffset(center, -3 * 1852, this.props.jumprunDirection()),
      google.maps.geometry.spherical.computeOffset(center, 3 * 1852, this.props.jumprunDirection())
    ];
    const smallCrossLine = {
      path: 'M 0,0 4,0',
      strokeWeight: 1,
    };
    const mediumCrossLine = {
      path: 'M 0,0 6,0',
      strokeWeight: 2,
    };
    const largeCrossLine = {
      path: 'M 0,0 8,0',
      strokeWeight: 3,
    };
    return new google.maps.Polyline({
      path: jumpRunEnds,
      icons: [{
        icon: mediumCrossLine,
        offset: '0',
        repeat: '8.3333333%'
      },{
        icon: smallCrossLine,
        offset: '0',
        repeat: '1.6666666%'
      },{
        icon: largeCrossLine,
        offset: '0',
        repeat: '16.6666666%'
      }],
      strokeColor: '#ffffff',
      strokeOpacity: 1.0,
      strokeWeight: 2,
      map: this.map
    });
  }

  dz() {
    return new google.maps.LatLng(this.props.dropzone.lat, this.props.dropzone.lng)
  }

  drawHelsinkiAtcBorders() {
    var hyppyAlueMalmiPisteet = [
      new google.maps.LatLng(60.282643,25.029259),
      new google.maps.LatLng(60.277877,25.133801),
      new google.maps.LatLng(60.208696,25.10951),
      new google.maps.LatLng(60.217053,24.980249), //arabian kulma
      new google.maps.LatLng(60.242495,24.993382),
      new google.maps.LatLng(60.282643,25.029259)
    ];
    var hyppyAlueMalmi = new google.maps.Polyline({
      path: hyppyAlueMalmiPisteet,
      strokeColor: "#FF0000",
      strokeOpacity: 1.0,
      strokeWeight: 5
    });
    hyppyAlueMalmi.setMap(this.map);

    var hyppyAlueKeskustaPisteet = [
      new google.maps.LatLng(60.217053,24.980249), // vanhankaupunginkoski
      new google.maps.LatLng(60.185745,25.012436), // kulosaari
      new google.maps.LatLng(60.154065,24.960079), //kaivari
      //new google.maps.LatLng(60.143727,24.873648),
      //new google.maps.LatLng(60.169653,24.821291),
      //new google.maps.LatLng(60.217309,24.815025),
      new google.maps.LatLng(60.202768,24.906561),
      new google.maps.LatLng(60.208227,24.920037),
      new google.maps.LatLng(60.206179,24.927721),
      new google.maps.LatLng(60.217053,24.980249),
      new google.maps.LatLng(60.214917,25.012138), //lammassaari
      new google.maps.LatLng(60.185745,25.012436) // kulosaari
    ];
    var hyppyAlueKeskusta = new google.maps.Polyline({
      path: hyppyAlueKeskustaPisteet,
      strokeColor: "#FF0000",
      strokeOpacity: 1.0,
      strokeWeight: 5
    });
    hyppyAlueKeskusta.setMap(this.map);
  }

  startGpsTracking() {
    this.gpsWatchId = navigator.geolocation.watchPosition((event) => {
        const currentLocation = {lat: event.coords.latitude, lng: event.coords.longitude};
        this.map.panTo(currentLocation);
        const airplaneSymbol = {
          path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
          fillColor: '#FFFFFF',
          strokeColor: '#ff4000',
          fillOpacity: 1,
          strokeWeight: 3,
          scale: 7,
          rotation: event.coords.heading
        };
        this.airplane && this.airplane.setMap(null);
        this.airplane = new google.maps.Marker({
          position: currentLocation,
          icon: airplaneSymbol,
          map: this.map
        });
      },
      (unSuccessfull) => 0,
      {
        enableHighAccuracy:true,
        maximumAge:30000,
        timeout:20000
      }
    );
  }

  stopGpsTracking() {
    navigator.geolocation.clearWatch(this.gpsWatchId);
  }

}

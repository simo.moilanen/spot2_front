import React from 'react';
import { Menu } from 'semantic-ui-react'

export default class DropzoneSelector extends React.Component {
  render() {
    return (
      <div>
        <Menu fluid vertical className="dropzone-selector">
          {this.props.dropzones.map((dz) =>
            <Menu.Item
              key={dz.id}
              name={dz.name}
              active={this.props.selectedDropzone.id === dz.id}
              onClick={() => this.props.onSelectDropzone(dz)}
            />
          )}
        </Menu>
        Check out <a href='http://35.198.179.1/'>Spot3</a>!!
      </div>
    );
  }
}

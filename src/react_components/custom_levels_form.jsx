import React from 'react';
import { Table, Input, Icon, Button } from 'semantic-ui-react'
import { isNumeric } from '../models/helpers.js'

export default class CustomLevelsForm extends React.Component {
  render() {
    return (
      <Table singleLine className="custom-winds-form" unstackable>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Height<br/>(m)</Table.HeaderCell>
            <Table.HeaderCell>Dir<br/>(°)</Table.HeaderCell>
            <Table.HeaderCell>Speed<br/>(m/s)</Table.HeaderCell>
            <Table.HeaderCell></Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {this.props.customLevels.map((level) =>
            <Table.Row
              key={level.id}
              onChange={(event) => this.props.onCustomLevelInputChange(level.id, event.target.name, event.target.value)}>
              <Table.Cell>
                <Input
                  name="height_m"
                  value={isNumeric(level.height_m) ? level.height_m : ''}
                  type="number"
                  pattern="\d*"
                  min="0"
                  step="100"
                  style={{width: '76px'}}
                />
              </Table.Cell>
              <Table.Cell>
                <Input
                  name="wind_direction_deg"
                  value={isNumeric(level.wind_direction_deg) ? level.wind_direction_deg : ''}
                  type="number"
                  pattern="\d*"
                  min="0"
                  step="1"
                  style={{width: '67px'}}
                />
              </Table.Cell>
              <Table.Cell>
                <Input
                  name="wind_speed_ms"
                  value={isNumeric(level.wind_speed_ms) ? level.wind_speed_ms : ''}
                  type="number"
                  pattern="\d*"
                  min="0"
                  step="0.5"
                  style={{width: '59px'}}
                />
              </Table.Cell>
              <Table.Cell>
                {level.allValuesNumeric() &&
                  <Button icon color="red" size="tiny" onClick={() => this.props.removeCustomLevelRow(level.id)}>
                    <Icon name="remove" />
                  </Button>
                }
              </Table.Cell>
            </Table.Row>
          )}
        </Table.Body>
      </Table>
    )
  }
}


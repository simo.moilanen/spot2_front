import React from 'react';
import { Segment, Table } from 'semantic-ui-react'
import { isNumeric, levelSorter } from '../models/helpers.js'

export default class LevelsTable extends React.Component {

  render() {
    const sortedLevels = this.props.levels.sort(levelSorter);
    return (
      <Table singleLine unstackable>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Height<br/>(m)</Table.HeaderCell>
            <Table.HeaderCell>Dir<br/>(°)</Table.HeaderCell>
            <Table.HeaderCell>Speed<br/>(m/s)</Table.HeaderCell>
            <Table.HeaderCell>Temp.<br/>(°C)</Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {sortedLevels.map((level) =>
            <Table.Row key={level.id}>
              <Table.Cell>{isNumeric(level.height_m) && Math.round(level.height_m).toString()}</Table.Cell>
              <Table.Cell>{isNumeric(level.wind_direction_deg) && Math.round(level.wind_direction_deg).toString()}</Table.Cell>
              <Table.Cell>{isNumeric(level.wind_speed_ms) && Math.round(level.wind_speed_ms).toString()}</Table.Cell>
              <Table.Cell>{isNumeric(level.temperature_c) && Math.round(level.temperature_c).toString()}</Table.Cell>
            </Table.Row>
          )}
        </Table.Body>
      </Table>
    )
  }

}
import React from 'react';
import { Icon } from 'semantic-ui-react'
import { isNumeric } from '../models/helpers.js'

export default class MapLegend extends React.Component {
  render() {
    const jumpRunDirection = Math.round(this.props.jumprunDirection());
    return (
      <div id="map-legend">
        {isNumeric(jumpRunDirection) && <div>Jumprun direction: {jumpRunDirection}° (true) {(360 + jumpRunDirection - 8) % 360}° (mag)</div>}
        {this.props.jumpers.map((jumper) =>
          <div key={jumper.id}>
            <Icon name="circle thin" style={{color: jumper.color}} size="big" />
            {jumper.name}
          </div>
        )}
      </div>
    )
  }
}

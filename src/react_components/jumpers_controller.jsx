import React from 'react';
import { Button, Icon, Form, Segment, Popup, Label } from 'semantic-ui-react'
import { CirclePicker } from 'react-color';
import { InstructionPopup } from './instruction_popup.jsx'

export default class JumpersController extends React.Component {
  render() {
    return (
      <div className="dropzone-controller">
        {this.props.jumpers.map((jumper) =>
          <Segment key={jumper.id}>
            {jumper.id > 1 &&
            <Label as='a' color='red' corner='right' onClick={() => this.props.onJumperRemove(jumper.id)}>
              <Icon name="remove"/>
            </Label>}
            <Form onChange={(event) => this.props.onJumperChange(jumper.id, event.target.name, event.target.value)} >
              <Form.Input name="name" label="Name" value={jumper.name} />
              <Form.Field>
                <label>
                  Open altitude (m)
                  <InstructionPopup
                    header='Open altitude'
                    content='The altitude where canopy is fully open. For example if pulling height is 1000m and parachute
                  opening is expected to take 200m, open height would be 800m. In the calculation it is assumed that
                  jumper is transfered from freefall to canopy flight instantly at open height so deceleration during
                  parachute opening is not taken into account.'
                  />
                </label>
                <input name="openAlt" value={jumper.openAlt} type="number" pattern="\d*" min="0" step="100" />
              </Form.Field>
              <Form.Field>
                <label>
                  Canopy descent rate (m/s)
                  <InstructionPopup
                    header='Canopy descent rate'
                    content='Vertical speed under canopy. For a tandem this is usually around 4 m/s. This number increases
                  with wingloading.'
                  />
                </label>
                <input name="canopyDescRate" label="Canopy descent rate" value={jumper.canopyDescRate} type="number"
                       pattern="\d*[,\.]{0,1}\d*" min="0" step="0.1" />
              </Form.Field>
              <Form.Field>
                <label>
                  Canopy glide ratio
                  <InstructionPopup
                    header='Canopy glide ratio'
                    content='The distance a parachute travels horizontally when
                  descending one meter. For most parachutes this number is between 2 and 3.'
                  />
                </label>
                <input name="canopyGlideAngle" label="Canopy glide ratio" value={jumper.canopyGlideAngle} type="number"
                       pattern="\d*[,\.]{0,1}\d*" min="0" step="0.1" />
              </Form.Field>
              <Form.Field>
                <label>
                  Freefall speed (km/h)
                  <InstructionPopup
                    header='Freefall speed'
                    content='Usually ranges from 180 to 300 km/h depending on body position'
                  />
                </label>
                <input name="terminalVelocity_kmh" label="Freefall speed" value={Math.round(jumper.terminalVelocity_kmh())}
                       type="number" pattern="\d*" min="0" step="1" />
              </Form.Field>
              <Form.Field>
                <label>
                  Set-up altitude (m)
                  <InstructionPopup
                    header='Set-up altitude'
                    content='The altitude you wish to be on top of target. This could be used for example by a swooper
                  wishing to initiate their swoop at a certain height.'
                  />
                </label>
                <input name="setUpAlt" label="Set up altitude" value={jumper.setUpAlt} type="number" pattern="\d*" min="0"
                       step="10" />
              </Form.Field>
              <CirclePicker
                color={jumper.color}
                colors={['#FFFF00', '#00FF00', '#0000FF', '#00FFFF', '#FF00FF']}
                onChangeComplete={(color, event) => this.props.onJumperChange(jumper.id, 'color', color.hex)}
              />
            </Form>
          </Segment>
        )}
        <Button icon color="green" className="add-jumper-btn" onClick={this.props.onAddJumper}>
          <Icon name="user plus" size="big"/>
        </Button>
      </div>
    );
  }
}

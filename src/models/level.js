import { meters, metersPerSecond } from './helpers.js'
import { isNumeric } from '../models/helpers.js'

export default class {

  constructor(params={}) {
    this.id = params.id;
    this.wind_direction_deg = params.wind_direction_deg;
    this.wind_speed_ms = metersPerSecond(params.wind_speed_kt) || params.wind_speed_ms;
    this.temperature_c = params.temperature_c;
    if (Object.keys(params).includes('height_ft')) {
      this.height_m = meters(params.height_ft)
    } else {
      this.height_m = params.height_m;
    }
    if (Object.keys(params).includes('wind_speed_kt')) {
      this.wind_speed_ms = metersPerSecond(params.wind_speed_kt)
    } else {
      this.wind_speed_ms = params.wind_speed_ms;
    }
  }
  
  updateAttribute(attribute, value) {
    if (attribute == 'height_ft') {
      this.height_m = meters(value);
    } else if (attribute == 'wind_speed_kt') {
      this.wind_speed_ms = metersPerSecond(value);
    } else {
      this[attribute] = value;
    }
  }

  wind_direction_rad() {
    return (this.wind_direction_deg / 360) * 2 * Math.PI;
  }

  allValuesNumeric() {
    return isNumeric(this.height_m) && isNumeric(this.wind_direction_deg) && isNumeric(this.wind_speed_ms)
  }
}

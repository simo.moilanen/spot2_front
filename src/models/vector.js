import { radians } from './helpers.js'

export default class Vector {
  constructor(params={}) {
    if (Object.keys(params).includes('x') && Object.keys(params).includes('y')) {
      this.x = params.x;
      this.y = params.y;
    } else {  // includes dir and dist
      if ((params.dir >= 0 && params.dir <= 90) || params.dir == 360) {
        this.x = params.dist * Math.sin(radians(params.dir));
        this.y = params.dist * Math.cos(radians(params.dir));
      } else if (params.dir > 90 && params.dir <= 180) {
        this.x = params.dist * Math.sin(radians(180 - params.dir));
        this.y = -params.dist * Math.cos(radians(180 - params.dir));
      } else if (params.dir > 180 && params.dir < 270) {
        this.x = -params.dist * Math.sin(radians(params.dir - 180));
        this.y = -params.dist * Math.cos(radians(params.dir - 180));
      } else if (params.dir >= 270 && params.dir <= 360) {
        this.x = -params.dist * Math.sin(radians(360 - params.dir));
        this.y = params.dist * Math.cos(radians(360 - params.dir));
      }
    }
  }

  direction() {
    function degrees(value){ return (value/(2*Math.PI))*360 }
    if (this.y == 0) {
      if (this.x >= 0) { return 90; } else { return 270; }
    } else if (this.x >= 0 && this.y >0 ){
      return degrees(Math.atan(this.x/this.y))
    } else if (this.x > 0 && this.y <0 ){
      return 180 - degrees(Math.atan(this.x/Math.abs(this.y)))
    } else if (this.x <= 0 && this.y <0 ){
      return degrees(Math.atan(Math.abs(this.x/this.y))) + 180
    } else if (this.x < 0 && this.y >0 ){
      return 360 - degrees(Math.atan(Math.abs(this.x)/this.y))
    }
  }

  distance() {
    return Math.sqrt(this.x * this.x + this.y * this.y);
  }

  add(another) {
    return new Vector({x: this.x + another.x, y: this.y + another.y});
  }
  
  mul(constant) {
    return new Vector({x: this.x * constant, y: this.y * constant});
  }

  sub(another) {
    return new Vector({x: this.x - another.x, y: this.y - another.y});
  }
}

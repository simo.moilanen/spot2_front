import { meters, metersPerSecond, knots, radians, isNumeric } from './helpers.js'

export default class {
  constructor(params = {}) {
    Object.keys(params).forEach((key) => {
      if (key == 'height_ft') {
        this.height_m = meters(params[key]);
      } else if (key == 'ias_kt') {
        this.ias_ms = metersPerSecond(params[key]);
      } else {
        this[key] = params[key];
      }
    })
  }

  tas_ms() {
    const g = 9.81;
    const h = this.height_m;
    const standard_density = 1.293;
    const p0 = 101325;
    const L = 0.0065;
    const R = 8.31447;
    const M = 0.0289644;
    const T0 = 288.15;
    const density_os = M * p0 * Math.pow((1 - (L * h) / T0),(g * M)/(R * L));
    const density_nim = R * (T0 - L * h);
    const density = density_os / density_nim;
    return this.ias_ms * Math.sqrt(standard_density/density);
  }
  
  tas_kt() {
    return knots(this.tas_ms());
  }

  ias_kt() {
    return isNumeric(this.ias_ms) ? knots(this.ias_ms) : '';
  }

  updateAttribute(attribute, value) {
    if (attribute == 'height_ft') {
      this.height_m = isNumeric(value) ? meters(value) : '';
    } else if (attribute == 'ias_kt') {
      this.ias_ms = isNumeric(value) ? metersPerSecond(value) : '';
    } else {
      this[attribute] = value;
    }
  }

  jumpRunLevel(levels) {
    return levels.sort((a, b) => {
      const aDifferce = Math.abs(a.height_m - this.height_m);
      const bDifferce = Math.abs(b.height_m - this.height_m);
      if (aDifferce > bDifferce) { return 1; }
      if (aDifferce < bDifferce) { return -1; }
      return 0;
    })[0];
  }

  groundSpeed_ms(levels, jumpRunDirection) {
    const w = radians(this.jumpRunLevel(levels).wind_direction_deg); // tuulen suunta (rad)
    const Vw = this.jumpRunLevel(levels).wind_speed_ms; // tuulen nopeus (m/s)
    const da = Math.asin((Vw * Math.sin(w-radians(jumpRunDirection)))/this.tas_ms());
    const Va = this.tas_ms();
    return Math.sqrt(Va*Va + Vw*Vw - 2 * Va * Vw * Math.cos(radians(jumpRunDirection) - w + da))
  }

  doorOpenDistance_m(levels, jumpRunDirection) {
    return this.groundSpeed_ms(levels, jumpRunDirection) * this.door_open_time
  }

  groupDistance_m(levels, jumpRunDirection) {
    return this.groundSpeed_ms(levels, jumpRunDirection) * this.group_separation_time
  }
}

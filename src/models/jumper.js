import Vector from './vector.js'
import { levelSorter, metersPerSecondFromKmh, kmhFromMetersPerSecond, isNumeric } from './helpers.js'

export default class {
  constructor(params={}) {
    Object.keys(params).forEach((key) => {
      this[key] = params[key];
    })
  }

  optimumSpot(levels, jumprun, jumprunDirection) {
    const throwVector = new Vector({dir: jumprunDirection, dist: this.throwDistance(jumprun)});
    return this.optimumSpotWithoutThrow(levels, jumprun).sub(throwVector);
  }

  optimumSpotWithoutThrow(levels, jumprun) {
    let ret = new Vector({x:0, y:0});
    this.segments(levels, jumprun).forEach((segment) => {
      const segmentWindVector = new Vector({
        dir: segment.wind_direction_deg,
        dist: segment.wind_speed_ms * this.timeSpentInSegment(segment, jumprun)
      });
      ret = ret.add(segmentWindVector);
    });
    return ret
  }

  timeSpentInSegment(segment, jumprun) {
    if (this.openAlt <= segment.bottom) {  // only freefall
      return this.timeSpentInSegmentFreefall(segment, jumprun);
    } else if (this.openAlt >= segment.top) {  // only under canopy
      return this.timeSpentInSegmentCanopy(segment);
    } else { // both
      const freefallSegment = {bottom: this.openAlt, top: segment.top};
      const canopySegment = {bottom: segment.bottom, top: this.openAlt};
      return this.timeSpentInSegmentFreefall(freefallSegment, jumprun) + this.timeSpentInSegmentCanopy(canopySegment)
    }
  }
  
  throwDistance(jumprun){
    const g = 9.81;
    if (this.openAlt >= jumprun.height_m){ return 0; }
    const freeFallTime = this.timeSpentInSegmentFreefall({top: jumprun.height_m, bottom: this.openAlt}, jumprun);
    return ((this.terminalVelocity*this.terminalVelocity*freeFallTime)/
            (g*(freeFallTime+(this.terminalVelocity*this.terminalVelocity)/(g*jumprun.tas_ms()))));
  }

  timeSpentInSegmentFreefall(segment, jumprun){
    const g = 9.81;
    const t_high = (Math.acosh(Math.pow(Math.E,((g*(jumprun.height_m-segment.top) /
      (this.terminalVelocity*this.terminalVelocity)))))*this.terminalVelocity)/g;
    const t_low = (Math.acosh(Math.pow(Math.E,((g*(jumprun.height_m-segment.bottom) /
      (this.terminalVelocity*this.terminalVelocity)))))*this.terminalVelocity)/g;
    return t_low-t_high;
  }

  timeSpentInSegmentCanopy(segment){
    return (segment.top-segment.bottom)/this.canopyDescRate
  }

  circleRadius(jumprun) {
    const canopyRideUpperLimit = Math.min(this.openAlt, jumprun.height_m);
    return (canopyRideUpperLimit - this.setUpAlt) * this.canopyGlideAngle;
  }

  segments(levels, jumprun) {
    let segments = [];
    const sortedLevels = levels.sort(levelSorter);
    for (let i = 0; i < sortedLevels.length; i++) {
      const level = sortedLevels[i];
      const nextLevel = sortedLevels[i + 1];
      const bottom = segments[segments.length-1] ? Math.max(segments[segments.length-1].top, this.setUpAlt) : this.setUpAlt;
      const top = nextLevel ? Math.min( ((level.height_m + nextLevel.height_m) / 2), jumprun.height_m) : jumprun.height_m;
      if (this.setUpAlt >= top) { continue; }
      segments.push({
        bottom: bottom,
        top: top,
        wind_speed_ms: level.wind_speed_ms,
        wind_direction_deg: level.wind_direction_deg
      });
      if (top == jumprun.height_m) { break; }
    }
    return segments.reverse();
  }

  updateAttribute(attribute, value) {
    if (attribute == 'terminalVelocity_kmh') {
      this.terminalVelocity = isNumeric(value) ? metersPerSecondFromKmh(value) : '';
    } else {
      this[attribute] = value;
    }
  }

  terminalVelocity_kmh() {
    return kmhFromMetersPerSecond(this.terminalVelocity);
  }
}

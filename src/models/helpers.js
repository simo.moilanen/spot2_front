export function meters(feet) {
  return feet * 0.3048;
}

export function metersPerSecond(knots) {
  return knots * 0.5144;
}

export function knots(metersPerSecond) {
  return metersPerSecond / 0.5144;
}

export function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

export function radians(value) {
  return (value/360)*2*Math.PI
}

export function levelSorter(a, b) {
  if (a.height_m > b.height_m) { return 1; }
  if (a.height_m < b.height_m) { return -1; }
  return 0;
}

export function metersPerSecondFromKmh(kmh) {
  return kmh / 3.6;
}

export function kmhFromMetersPerSecond(metersPerSecond) {
  return metersPerSecond * 3.6;
}
